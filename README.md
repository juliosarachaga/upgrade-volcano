## VOLCANO

### Run

You must have Docker installed on your computer

```
mvn package
docker-compose up
```

If something is changed and want to regenerate the container for the application
```
mvn package
docker-compose build
docker-compose up
```