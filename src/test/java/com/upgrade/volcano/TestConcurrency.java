package com.upgrade.volcano;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.upgrade.volcano.controller.dto.ReservationDto;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;

import java.time.LocalDate;
import java.util.Map;
import java.util.concurrent.*;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT, classes = VolcanoApplication.class)
@AutoConfigureMockMvc
@AutoConfigureTestDatabase
public class TestConcurrency {

    @Autowired
    private MockMvc mvc;

    @LocalServerPort
    int port;

    class CallableRequests implements Callable<MvcResult> {
        public MvcResult call() throws Exception {

            LocalDate startDate = LocalDate.now().plusDays(2);
            LocalDate endDate = startDate.plusDays(2);

            ReservationDto res = new ReservationDto();
            res.setStartDate(startDate);
            res.setEndDate(endDate);
            res.setFullName("Julio Sarachaga");
            res.setMail("jsarachaga@gmail.com");

            ObjectMapper om = new ObjectMapper();
            JsonNode json = om.convertValue(res, JsonNode.class);

            ResultActions response = mvc.perform(post("/reservations").header("host", "localhost:" + port).contentType(MediaType.APPLICATION_JSON_VALUE).accept(MediaType.APPLICATION_JSON_VALUE).content(json.toString()));
            return response.andReturn();
        }
    }

    @Test
    public void testOneErrorWhenMultipleRequestForTheSameDate() {

        int size = 20;
        ExecutorService service = Executors.newFixedThreadPool(size);
        CompletionService<MvcResult> completionService =
                new ExecutorCompletionService<MvcResult>(service);

        for (int i = 0; i < size; i++) {
            CallableRequests callable = new CallableRequests();
            completionService.submit(callable);
        }

        Map<Integer, Integer> statuses = new ConcurrentHashMap<>();
        try {
            for (int i = 0; i < size; i++) {
                Future<MvcResult> future = completionService.take();

                Integer key = future.get().getResponse().getStatus();
                Integer val = 0;
                if (statuses.containsKey(key)) {
                    val = statuses.get(key);
                }

                statuses.put(key, val+1);
            }

            Assert.assertTrue("Both shouldn't have the same status.", statuses.get(200) == 1 && statuses.get(400) == size - 1);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
    }

}
