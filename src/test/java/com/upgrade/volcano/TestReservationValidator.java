package com.upgrade.volcano;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.upgrade.volcano.controller.dto.ReservationDto;
import com.upgrade.volcano.controller.validation.ReservationValidator;
import javafx.beans.binding.When;
import org.apache.tomcat.jni.Local;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;

import javax.validation.ConstraintValidatorContext;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.time.LocalDate;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT, classes = VolcanoApplication.class)
@AutoConfigureMockMvc
@AutoConfigureTestDatabase
public class TestReservationValidator {

    @Autowired
    private MockMvc mvc;

    @LocalServerPort
    int port;

    private ObjectMapper om;

    @Before
    public void beforeEach() {
        om = new ObjectMapper();
    }

    private MvcResult doRequest(LocalDate startDate, LocalDate endDate) throws Exception {
        ReservationDto res = new ReservationDto();
        res.setStartDate(startDate);
        res.setEndDate(endDate);
        res.setFullName("Julio Sarachaga");
        res.setMail("jsarachaga@gmail.com");

        ObjectMapper om = new ObjectMapper();
        JsonNode json = om.convertValue(res, JsonNode.class);

        ResultActions response = mvc.perform(post("/reservations").header("host", "localhost:" + port).contentType(MediaType.APPLICATION_JSON_VALUE).accept(MediaType.APPLICATION_JSON_VALUE).content(json.toString()));
        return response.andReturn();
    }

    @Test
    public void testMinDaysToGoLessThan0() throws IOException {
        LocalDate startDate = LocalDate.now().minusDays(20);
        LocalDate endDate = startDate.plusDays(2);

        MvcResult result = null;
        try {
            result = doRequest(startDate, endDate);
        } catch (Exception e) {
            Assert.fail("Failed the request");
        }

        Assert.assertEquals(400, result.getResponse().getStatus());

        JsonNode json = om.readTree(result.getResponse().getContentAsString());
        Assert.assertEquals("The reservation is in the past.", json.get("message").asText());
    }

    @Test
    public void testMinDaysToGoLessThanBeforeReservation() throws IOException {
        LocalDate startDate = LocalDate.now();
        LocalDate endDate = startDate.plusDays(3);

        MvcResult result = null;
        try {
            result = doRequest(startDate, endDate);
        } catch (Exception e) {
            Assert.fail("Failed the request");
        }

        Assert.assertEquals(400, result.getResponse().getStatus());
        JsonNode json = om.readTree(result.getResponse().getContentAsString());
        Assert.assertEquals("The reservation must be made at least 1 day before arriving.", json.get("message").asText());
    }

    @Test
    public void testMindDaysToGoNotGreaterThanMaxDays() throws IOException {
        ReservationDto reservationDto = mock(ReservationDto.class);

        LocalDate startDate = LocalDate.now().plusMonths(2);
        LocalDate endDate = startDate.plusDays(3);

        MvcResult result = null;
        try {
            result = doRequest(startDate, endDate);
        } catch (Exception e) {
            Assert.fail("Failed the request");
        }

        Assert.assertEquals(400, result.getResponse().getStatus());
        JsonNode json = om.readTree(result.getResponse().getContentAsString());
        Assert.assertEquals("The reservation must be made at least 31 days before arriving.", json.get("message").asText());
    }

    @Test
    public void testMinDaysReserved() throws IOException {
        ReservationDto reservationDto = mock(ReservationDto.class);

        LocalDate startDate = LocalDate.now().plusDays(2);

        MvcResult result = null;
        try {
            result = doRequest(startDate, startDate);
        } catch (Exception e) {
            Assert.fail("Failed the request");
        }

        Assert.assertEquals(400, result.getResponse().getStatus());
        JsonNode json = om.readTree(result.getResponse().getContentAsString());
        Assert.assertEquals("The difference between the two dates provided is wrong, the minimum reservation time is greater than 1 and less than 3", json.get("message").asText());
    }

    @Test
    public void testMaxDaysReserved() throws IOException {
        ReservationDto reservationDto = mock(ReservationDto.class);

        LocalDate startDate = LocalDate.now().plusDays(2);
        LocalDate endDate = startDate.plusDays(4);

        MvcResult result = null;
        try {
            result = doRequest(startDate, endDate);
        } catch (Exception e) {
            Assert.fail("Failed the request");
        }

        Assert.assertEquals(400, result.getResponse().getStatus());
        JsonNode json = om.readTree(result.getResponse().getContentAsString());
        Assert.assertEquals("The difference between the two dates provided is wrong, the minimum reservation time is greater than 1 and less than 3", json.get("message").asText());
    }

    @Test
    public void testEverythingIsOkOneDay() throws IOException {
        ReservationDto reservationDto = mock(ReservationDto.class);

        LocalDate startDate = LocalDate.now().plusDays(20);
        LocalDate endDate = startDate.plusDays(3);

        MvcResult result = null;
        try {
            result = doRequest(startDate, endDate);
        } catch (Exception e) {
            Assert.fail("Failed the request");
        }

        Assert.assertEquals(200, result.getResponse().getStatus());
    }

    @Test
    public void testEverythingIsOkThreeDays() throws IOException {
        ReservationDto reservationDto = mock(ReservationDto.class);

        LocalDate startDate = LocalDate.now().plusDays(25);
        LocalDate endDate = startDate.plusDays(1);

        MvcResult result = null;
        try {
            result = doRequest(startDate, endDate);
        } catch (Exception e) {
            Assert.fail("Failed the request");
        }

        Assert.assertEquals(200, result.getResponse().getStatus());
    }
}