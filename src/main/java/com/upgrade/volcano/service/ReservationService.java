package com.upgrade.volcano.service;

import com.upgrade.volcano.controller.dto.CriteriaDto;
import com.upgrade.volcano.controller.dto.ReservationDto;
import org.springframework.data.domain.Pageable;

import java.time.LocalDate;
import java.util.List;

public interface ReservationService {

    ReservationDto createReservation(ReservationDto reservationDto);

    ReservationDto updateReservation(String id, ReservationDto reservationDto);

    void deleteReservation(String id);

    List<ReservationDto> searchReservationsBetweenDates(CriteriaDto criteriaDto, Pageable page);
}
