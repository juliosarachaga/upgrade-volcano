package com.upgrade.volcano.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.upgrade.volcano.controller.dto.CriteriaDto;
import com.upgrade.volcano.controller.dto.ReservationDto;
import com.upgrade.volcano.persistance.model.Reservation;
import com.upgrade.volcano.persistance.repository.ReservationRepository;
import com.upgrade.volcano.service.exception.InvalidReservationException;
import com.upgrade.volcano.service.exception.ReservationNotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Service
public class ReservationServiceImpl implements ReservationService {

    private ObjectMapper om;

    private ReservationRepository reservationRepository;

    public ReservationServiceImpl(ReservationRepository reservationRepository, ObjectMapper om) {
        this.om = om;
        this.reservationRepository = reservationRepository;
    }

    @Transactional
    private Reservation createOrUpdate(ReservationDto dto, Reservation reservation) {
        synchronized (this) {
            if (!reservationRepository.findReservationBetweenDatesByReservationId(dto.getStartDate(), dto.getEndDate(), reservation.getReservationId()).isEmpty()) {
                throw new InvalidReservationException("The date you're trying to reserve is already taken");
            }

            reservation.setMail(dto.getMail());
            reservation.setStartDate(dto.getStartDate());
            reservation.setEndDate(dto.getEndDate());
            reservation.setFullName(dto.getFullName());

            return reservationRepository.save(reservation);
        }
    }

    /**
     * Create a new reservation
     * @param reservationDto
     * @return
     */
    public ReservationDto createReservation(ReservationDto reservationDto) {
        Reservation reservation = new Reservation();
        Reservation savedReservation = createOrUpdate(reservationDto, reservation);

        return om.convertValue(savedReservation, ReservationDto.class);
    }

    /**
     * Update an existing reservation
     * @param id
     * @param reservationDto
     * @return
     */
    public ReservationDto updateReservation(String id, ReservationDto reservationDto) {
        Reservation reservation = reservationRepository.getOne(id);
        Reservation savedReservation = createOrUpdate(reservationDto, reservation);

        return om.convertValue(savedReservation, ReservationDto.class);
    }

    /**
     * Delete a reservation
     * @param id
     */
    public void deleteReservation(String id) {
        if (!reservationRepository.existsById(id)) {
            throw new ReservationNotFoundException("Reservation not found with id " + id.toString());
        }

        reservationRepository.deleteById(id);
    }

    /**
     * Search the reservations between two dates
     *
     * @param criteriaDto
     * @param page
     * @return
     */
    public List<ReservationDto> searchReservationsBetweenDates(CriteriaDto criteriaDto, Pageable page) {
        Page<Reservation> reservations = reservationRepository.findReservationBetweenDates(criteriaDto.getStartDate(), criteriaDto.getEndDate(), page);

        List<ReservationDto> reservationDtos = new ArrayList<>();
        for (Reservation reservation : reservations) {
            reservationDtos.add(om.convertValue(reservation, ReservationDto.class));
        }

        return reservationDtos;
    }
}
