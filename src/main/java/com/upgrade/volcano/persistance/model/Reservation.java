package com.upgrade.volcano.persistance.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;
import java.util.UUID;

@Entity
@Table(name = "reservations")
public class Reservation {

    @Id
    private String id;

    @JsonFormat(pattern = "yyyy-MM-dd", shape = JsonFormat.Shape.OBJECT)
    @Column(nullable = false)
    private LocalDate startDate;

    @JsonFormat(pattern = "yyyy-MM-dd", shape = JsonFormat.Shape.OBJECT)
    @Column(nullable = false)
    private LocalDate endDate;

    @Column(nullable = false)
    private String mail;

    @Column(nullable = false)
    private String fullName;

    public Reservation() {
        this.id = UUID.randomUUID().toString();
    }

    public String getReservationId() {
        return id;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }
}
