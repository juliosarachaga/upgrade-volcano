package com.upgrade.volcano.persistance.repository;

import com.upgrade.volcano.controller.dto.ReservationDto;
import com.upgrade.volcano.persistance.model.Reservation;
import org.apache.tomcat.jni.Local;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

public interface ReservationRepository extends JpaRepository<Reservation, String> {

    /**
     * Returns the reservation between the two dates specified with pagination

     * @param startDate
     * @param endDate
     * @param pageable
     * @return
     */
    @Query(value = "SELECT * FROM reservations WHERE (start_date >= :startDate AND end_date <= :endDate) OR (end_date >= :startDate AND start_date <= :startDate) OR (end_date > :endDate AND start_date < :endDate)",
            countQuery = "SELECT count(*) FROM reservations WHERE (start_date >= :startDate AND end_date <= :endDate) OR (end_date >= :startDate AND start_date < :startDate) OR (end_date > :endDate AND start_date < :endDate)",
            nativeQuery = true)
    Page<Reservation> findReservationBetweenDates(LocalDate startDate, LocalDate endDate, Pageable pageable);

    /**
     * Returns the List of Reservations between the two dates. If reservationId is specified filters for that reservation
     *
     * @param startDate
     * @param endDate
     * @param reservationId
     * @return
     */
    @Query(value = "SELECT * FROM reservations WHERE ((start_date >= :startDate AND end_date <= :endDate) OR (end_date >= :startDate AND start_date <= :startDate) OR (end_date > :endDate AND start_date < :endDate)) AND (:reservationId IS NULL OR :reservationId != id)",
            nativeQuery = true)
    List<Reservation> findReservationBetweenDatesByReservationId(LocalDate startDate, LocalDate endDate, String reservationId);

}
