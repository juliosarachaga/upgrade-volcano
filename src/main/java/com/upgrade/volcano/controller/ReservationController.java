package com.upgrade.volcano.controller;

import com.upgrade.volcano.controller.dto.CriteriaDto;
import com.upgrade.volcano.controller.dto.ReservationDto;
import com.upgrade.volcano.service.ReservationService;
import com.upgrade.volcano.service.ReservationServiceImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(value = "reservations", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class ReservationController {

    private ReservationService reservationService;

    public ReservationController(ReservationService reservationService) {
        this.reservationService = reservationService;
    }

    @PostMapping
    public ResponseEntity<?> createReservation(@Valid @RequestBody ReservationDto reservationDto) {
        ReservationDto savedReservation;
        savedReservation = reservationService.createReservation(reservationDto);

        return new ResponseEntity<>(savedReservation, HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updateReservation(@PathVariable("id") UUID id, @Valid @RequestBody ReservationDto reservationDto) {

        ReservationDto savedReservation;
        savedReservation = reservationService.updateReservation(id.toString(), reservationDto);

        return new ResponseEntity<>(savedReservation, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ReservationDto> deleteReservation(@PathVariable("id") UUID id) {
        reservationService.deleteReservation(id.toString());

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @GetMapping
    public List<ReservationDto> searchByDates(@RequestBody(required = false) CriteriaDto criteriaDto,
            @PageableDefault Pageable pageable) {

        if (criteriaDto == null) {
            LocalDate date = LocalDate.now();
            criteriaDto = new CriteriaDto(date, date.plusMonths(1));
        }


        List<ReservationDto> reservationDtos = reservationService.searchReservationsBetweenDates(criteriaDto, pageable);
        // sanitaize data so the email, id, and name are not shown
        for (ReservationDto reservationDto :
                reservationDtos) {
            reservationDto.setFullName(null);
            reservationDto.setReservationId(null);
            reservationDto.setMail(null);
        }

        return reservationDtos;

    }

}
