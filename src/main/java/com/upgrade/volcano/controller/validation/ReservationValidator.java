package com.upgrade.volcano.controller.validation;

import com.upgrade.volcano.controller.dto.ReservationDto;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public class ReservationValidator implements ConstraintValidator<ValidReservation, ReservationDto> {

    private static final int MAX_VALID = 3;

    private static final int MIN_VALID = 1;

    private static final int MIN_DAYS_TO_ARRIVAL_BEFORE_RESERVATION = 1;

    private static final int MAX_DAYS_TO_ARRIVAL_BEFORE_RESERVATION = 31;

    @Override
    public boolean isValid(ReservationDto reservationDto, ConstraintValidatorContext context) {
        LocalDate startDate = reservationDto.getStartDate();
        LocalDate endDate = reservationDto.getEndDate();

        LocalDate today = LocalDate.now();
        long minDaysToGo = today.until(startDate, ChronoUnit.DAYS);

        context.disableDefaultConstraintViolation();
        if (minDaysToGo < 0) {
            context.buildConstraintViolationWithTemplate("The reservation is in the past.")
                    .addConstraintViolation();
            return false;
        }

        if (minDaysToGo < MIN_DAYS_TO_ARRIVAL_BEFORE_RESERVATION) {
            context.buildConstraintViolationWithTemplate("The reservation must be made at least " + MIN_DAYS_TO_ARRIVAL_BEFORE_RESERVATION + " day before arriving.")
                    .addConstraintViolation();
            return false;
        }

        if (minDaysToGo > MAX_DAYS_TO_ARRIVAL_BEFORE_RESERVATION ) {
            context.buildConstraintViolationWithTemplate("The reservation must be made at least " + MAX_DAYS_TO_ARRIVAL_BEFORE_RESERVATION + " days before arriving.")
                    .addConstraintViolation();
            return false;
        }

        long days = startDate.until(endDate, ChronoUnit.DAYS);

        if (!((days <= MAX_VALID) && (days >= MIN_VALID))) {
            context.buildConstraintViolationWithTemplate("The difference between the two dates provided is wrong, the minimum reservation time is greater than " + MIN_VALID + " and less than " + MAX_VALID)
                    .addConstraintViolation();
            return false;
        }

        return true;
    }
}
