package com.upgrade.volcano.controller.error.response;

import java.time.ZonedDateTime;

public class ExceptionResponse {

    private ZonedDateTime timestamp;

    private String message;

    private String details;

    private String httpCodeMessage;

    public ExceptionResponse(ZonedDateTime timestamp, String message, String details,String httpCodeMessage) {
        super();
        this.timestamp = timestamp;
        this.message = message;
        this.details = details;
        this.httpCodeMessage=httpCodeMessage;
    }

    public ZonedDateTime getTimestamp() {
        return timestamp;
    }

    public String getMessage() {
        return message;
    }

    public String getDetails() {
        return details;
    }

    public String getHttpCodeMessage() {
        return httpCodeMessage;
    }
}