package com.upgrade.volcano.controller.error.response;

import com.upgrade.volcano.service.exception.InvalidReservationException;
import com.upgrade.volcano.service.exception.ReservationNotFoundException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.ZonedDateTime;
import java.util.List;

@ControllerAdvice
@RestController
public class CustomizedResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(ReservationNotFoundException.class)
    public final ResponseEntity<ExceptionResponse> handleNotFoundException(ReservationNotFoundException ex, WebRequest request) {
        ExceptionResponse exceptionResponse = new ExceptionResponse(ZonedDateTime.now(), ex.getMessage(),
                request.getDescription(false), HttpStatus.NOT_FOUND.getReasonPhrase());
        return new ResponseEntity<ExceptionResponse>(exceptionResponse, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({InvalidReservationException.class})
    public final ResponseEntity<ExceptionResponse> handleNotFoundException(InvalidReservationException ex, WebRequest request) {
        ExceptionResponse exceptionResponse = new ExceptionResponse(ZonedDateTime.now(), ex.getMessage(),
                request.getDescription(false), HttpStatus.BAD_REQUEST.getReasonPhrase());
        return new ResponseEntity<ExceptionResponse>(exceptionResponse, HttpStatus.BAD_REQUEST);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {

        List<ObjectError> errors = ex.getBindingResult().getAllErrors();

        if (errors.isEmpty()) {
            ExceptionResponse exceptionResponse = new ExceptionResponse(ZonedDateTime.now(), ex.getMessage(),
                    request.getDescription(false), HttpStatus.BAD_REQUEST.getReasonPhrase());

            return this.handleExceptionInternal(ex, exceptionResponse, headers, status, request);
        }

        ObjectError error = errors.get(0);

        ExceptionResponse exceptionResponse = new ExceptionResponse(ZonedDateTime.now(), error.getDefaultMessage(),
                request.getDescription(false), HttpStatus.BAD_REQUEST.getReasonPhrase());
        
        return this.handleExceptionInternal(ex, exceptionResponse, headers, status, request);
    }
}